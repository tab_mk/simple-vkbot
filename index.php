<?php 

// Проверкка наличия HTTP-запроса
if (!isset($_REQUEST)) { 
  return; 
} 

// Присваиваем декодированный запрос к нашему серверу переменной
$data = json_decode(file_get_contents('php://input')); 

// Подключаем конфиг
require('config.php');

// Функция отправки сообщения
function sendMsg($txt,$id){
	// Объявляем переменные из конфига
	global $token,$vkapi_version;
	// Создаем массив с параметрами
	$request_params = array( 
		'message' => $txt, 
		'user_id' => $id, 
		'access_token' => $token, 
		'v' => $vkapi_version 
	); 
	// Превращаем массив в строку URL-формата
	$get_params = http_build_query($request_params); 
	// Делаем запрос к методу messages.send с нашими параметрами
	file_get_contents('https://api.vk.com/method/messages.send?'. $get_params);
}

// Обрабатываем полученные события
switch ($data->type) { 
	// Обрабатываем запрос на подтверждения сервера
	case 'confirmation':
		// Выводим наш код
		echo $confirmation_token; 
		break;
	// Обработка новых сообщений
	case 'message_new':
		// Достаем из запроса ID пользователя и текст сообщения и присваиваем их переменным
		$user_id = $data->object->user_id;
		$text = $data->object->body;
		// Проверяем, состоит ли пользователь в нашей группе
		$user_info = json_decode(file_get_contents("https://api.vk.com/method/groups.isMember?user_id={$user_id}&group_id={$groupID}&v={$vkapi_version}")); 
		$isMember = $user_info->response;
		// Если состоит - переходим к обработке сообщения
		if($isMember){
			// Проверяем соответствие текста сообщения с командами.
			// Если сходится - присваиваем нужный текст переменной $mess
			// Если нет - присваиваем текст ошибки
			switch ($text) {
				case "ping":
					$mess = "pong";
					break;
				case "test":
					$mess = "тест!";
					break;
				default:
					$mess = $msg_onError;
					break;
			}
		// Если не состоит - выводим оповещение об этом
		}else{
			$mess = $msg_notSub;
		}
		break;
	// Обработка события выхода из группы
	case 'group_leave':
		$mess = $msg_onLeave;
		break;
	// Обработка события вступления в группу
	case 'group_join':
		$mess = $msg_onJoin;
		break;
}

// Проверка объявления переменной $mess и если она есть - вызываем функцию sendMsg
if(isset($mess)) sendMsg($mess,$data->object->user_id);

// Обязательное действие - вывод строки "ok", чтобы дать вк знать, что скрипт закончил работу
echo "ok";

?>