<?php

$token = ""; // Ключ доступа
$confirmation_token = ""; // Код подтверждения сервера
$groupID = 123; // ID вашей группы

$vkapi_version = "5.65"; // Версия VK API

// Тексты сообщений

$msg_onJoin = "Привет.";
$msg_onLeave = "Ой, все.";
$msg_onError = "Доступные команды:\n- ping\n- test";
$msg_notSub = "Чтобы меня использовать, ты должен быть подписан на \n 💠 vk.com/tab_group 💠";

?>